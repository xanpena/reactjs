import React, { PropsWithChildren } from 'react'
import Navbar from '@components/Navbar'

type LayoutProps = {
    children?: React.ReactNode
}

export default function Layout({ children }: LayoutProps) {
    return (
        <>
            <header>
                <Navbar/>
            </header>
            <main>
                { children }
            </main>
            <footer>

            </footer>
        </>
    )
}