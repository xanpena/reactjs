import Layout from '@components/Layout'
import { AppProps } from 'next/app'

const App = ({ Component, pageProps }: AppProps) => {

  return (
    <>
      <Layout />
      <Component {...pageProps} />
    </>
  )
}

export default App
