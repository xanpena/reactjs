import { NextApiRequest, NextApiResponse } from "next"

export default function all(request: NextApiRequest, response: NextApiResponse) {
    return response.end(JSON.stringify({title: 'Product 1'}))
}