import { useState, useEffect } from "react";
import getProducts from "../services/getProducts";

export default function useProducts() {
  const [products, setProducts] = useState([])


  useEffect(function() {
    getProducts()
      .then(products => {
        setProducts(products)
      })
  }, [])

  return { products }
}
