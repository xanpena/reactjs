import { useState } from "react";

export default function useShoppingCart() {

  const [shoppingCart, setShoppingCart] = useState([])
  const [isSlideOverOpen, setIsSlideOverOpen] = useState(0)

  const addToShoppingCart = (id, title, price) => {
    const newShoppingCart = [...shoppingCart]

    const product = shoppingCart.find(item => item.id == id)

    if (!product) {
      const newProduct = {
        id,
        title,
        price,
        quantity: 1
      }
      newShoppingCart.push(newProduct)
    } else {
      product.quantity = product.quantity + 1
    }

    setShoppingCart(newShoppingCart)
  }

  const removeFromShoppingCart = (id) => {
    const newShoppingCart = [...shoppingCart]
    const productIndex = newShoppingCart.findIndex(item => item.id == id)
    newShoppingCart.splice(productIndex, 1)
    setShoppingCart(newShoppingCart)
  }

  const handleSlideOver = () => {
    setIsSlideOverOpen(!isSlideOverOpen)
  }

  const totalProducts = shoppingCart.length
  const totalPrice = shoppingCart.reduce((sum, product) => sum + (product.price * product.quantity), 0)

  return {
    addToShoppingCart,
    removeFromShoppingCart,
    shoppingCart,
    totalProducts,
    handleSlideOver,
    isSlideOverOpen,
    totalPrice
  }
}
