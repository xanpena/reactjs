
export default function Image({src, alt}) {
  return (
    <figure>
      <img className="w-full" src={src} alt={alt} />
    </figure>
  )
}
