
export default function H2({ title }) {
  return (
    <h2>{ title }</h2>
  )
}
