5
export default function H6({ title }) {
  return (
    <h6>{ title }</h6>
  )
}
