
export default function H4({ title }) {
  return (
    <h4>{ title }</h4>
  )
}
