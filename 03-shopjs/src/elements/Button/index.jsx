
export default function Button({children, onClick}) {
  return (
    <button
      className='btn btn-blue rounded-full'
      onClick={onClick}
    >
      {children}
    </button>
  )
}
