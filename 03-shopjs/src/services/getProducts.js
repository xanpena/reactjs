
export default function getProducts() {
  const apiURL = 'https://fakestoreapi.com/products'

  return fetch(apiURL)
    .then(res => res.json())
    .then(response => response)
}
