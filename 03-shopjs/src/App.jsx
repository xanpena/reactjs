import './App.css'
import Layout from './components/Layout'
import { ShoppingCartContextProvider } from './context/ShoppingCartContext'

function App() {
  return (
    <>
    <ShoppingCartContextProvider>
      <Layout />
    </ShoppingCartContextProvider>
    </>
  )
}

export default App
