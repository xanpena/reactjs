import React, { useState } from 'react'
import useShoppingCart from '../hooks/useShoppingCart'

const Context = React.createContext({})

export function ShoppingCartContextProvider({ children }) {

  const {
    addToShoppingCart,
    removeFromShoppingCart,
    shoppingCart,
    totalProducts,
    handleSlideOver,
    isSlideOverOpen,
    totalPrice
  } = useShoppingCart()

  return <Context.Provider value={{
    addToShoppingCart,
    removeFromShoppingCart,
    shoppingCart,
    totalProducts,
    handleSlideOver,
    isSlideOverOpen,
    totalPrice
  }}>
    { children }
  </Context.Provider>
}

export default Context
