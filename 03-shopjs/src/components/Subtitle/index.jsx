import H2 from "../../elements/Headings/H2";

export default function Subtitle({ text }) {
  return (
    <H2 title={ text } />
  )
}
