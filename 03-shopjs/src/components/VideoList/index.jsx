import Video from "../Video";

export default function VideoList() {
  return (
    <section>
      <h2>Video List</h2>
      <Video />
    </section>
  )
}
