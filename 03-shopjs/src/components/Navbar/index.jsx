import { useContext } from 'react'
import Context from '../../context/ShoppingCartContext'
import { ShoppingCartIcon } from '@heroicons/react/24/solid'
import Button from '../Button';
import Menu from '../Menu';

export default function Navbar () {
  const activeStyle = 'underline underline-offset-4'

  const { totalProducts, handleSlideOver } = useContext(Context)

  return (
    <nav className="flex justify-between items-center fixed top-0 z-10 w-full py-5 px-8 text-sm font-light bg-white border-b border-slate-900/10 lg:px-8 lg:border-0 mx-4 lg:mx-0">
      <ul className="flex items-center gap-3">
        <li className="font-semibold">
          <Menu to='/' text='Home' />
        </li>
        <li>
          <Menu to='/products' text='Products' />
        </li>
        <li>
          <Menu to='/about-us' text='About us' />
        </li>
      </ul>
      <ul className="flex items-center gap-3">
        <li>
          <Menu to='/sign-in' text='Sign in' />
        </li>
        <li>
          <Menu to='/sign-un' text='Sign up' />
        </li>
        <li>
          <Button onClick={handleSlideOver}>
            <ShoppingCartIcon className="h-4 w-4"  />
          </Button>
          { totalProducts }
        </li>
      </ul>
    </nav>
  )
}
