import H1 from "../../elements/Headings/H1";

export default function Title({ text }) {
  return (
    <H1 title={ text } />
  )
}
