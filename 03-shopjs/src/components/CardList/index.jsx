
export default function CardList ({ children }) {

  return (
    <section className='flex flex-wrap mt-8 gap-3'>
      { children }
    </section>
  )
}
