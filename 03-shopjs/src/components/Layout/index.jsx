import { useRoutes, BrowserRouter } from 'react-router-dom'
import Navbar from "../Navbar";
import Home from './../../pages/Home'
import Login from './../../pages/Login'
import Orders from './../../pages/Orders'
import Register from './../../pages/Register'
import ErrorPage from './../../pages/ErrorPage'
import ProductDetail from '../../pages/ProductDetail';
import SlideOver from '../SlideOver';

const AppRoutes = () => {
  let routes = useRoutes([
    { path: '/', element: <Home /> },
    { path: '/sign-in', element: <Login /> },
    { path: '/sign-up', element: <Register /> },
    { path: '/products/:id', element: <ProductDetail /> },
    { path: '/my-orders', element: <Orders /> },
    { path: '/*', element: <ErrorPage /> },
  ])

  return routes
}

export default function Layout () {
  return (
    <>
      <BrowserRouter>
        <header>
          <Navbar />
        </header>
        <main className='flex flex-col mt-20 py-5 px-8'>
          <AppRoutes />
          <SlideOver />
        </main>
      </BrowserRouter>
      <footer></footer>
    </>
  )
}
