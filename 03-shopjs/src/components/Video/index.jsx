import Thumbnail from "../Thumbnail";

export default function Video() {
  return (
    <div>
      <Thumbnail />
      <a href="">
        <h3>title</h3>
        <p>description</p>
      </a>
    </div>
  )
}
