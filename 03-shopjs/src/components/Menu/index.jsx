
import { NavLink } from "react-router-dom";

export default function Menu({to, text}) {
  return (
    <NavLink
      to={ to }
      className={({ isActive }) => isActive ? activeStyle : undefined}
    >
      { text }
    </NavLink>
  )
}
