import { useContext } from "react";
import { Link } from "react-router-dom";
import Context from "../../context/ShoppingCartContext";
import { XMarkIcon } from "@heroicons/react/24/solid";
import Button from "../../elements/Button";
import H3 from "../../Headings/H3";

export default function SlideOver() {
  const { handleSlideOver, isSlideOverOpen, shoppingCart, removeFromShoppingCart, totalPrice } = useContext(Context);

  return (
    <aside className={`${isSlideOverOpen ? "fixed" : "hidden"} right-0 w-350 bg-white h-[calc(100vh-20px)] top-16 rounded shadow-lg px-5`}>
      <div className="flex justify-between items-center p-2">
        <H3 title="Shopping Cart" />
        <Button onClick={handleSlideOver}>
          <XMarkIcon className="h-4 w-4" />
        </Button>
      </div>
      <p className="font-medium text-2xl">Total: {totalPrice} $</p>
      <ul>
        {shoppingCart?.map((product) => (
          <li className="flex items-top my-5" key={product.id}>
            <figure className="w-[20%]">
              <img src="https://random.imagecdn.app/250/250" />
            </figure>
            <div className="mx-2">
              <h5>{product.title}</h5>
              <p className="w-[80%] font-medium text-2xl">{product.price} $</p>
              <p>Quantity: {product.quantity}</p>
              <Button onClick={() => removeFromShoppingCart(product.id)}>Quitar del carrito</Button>
            </div>
          </li>
        ))}
      </ul>
      <Link to="/my-orders">Checkout</Link>
    </aside>
  );
}
