import { useContext } from "react";
import { Link } from "react-router-dom";
import Context from "../../context/ShoppingCartContext";
import Button from "../../elements/Button";
import Image from "../../elements/Image";
import Budget from "../Budget";

export default function Card({ id, title, description, price, css }) {
  const { addToShoppingCart } = useContext(Context);
  return (
    <div className={css ?? "max-w-sm rounded overflow-hidden shadow-lg"}>
      <Image src="https://random.imagecdn.app/500/150" alt="Sunset in the mountains" />
      <div className="px-6 py-4">
        <h2 className="font-bold text-xl mb-2">{title}</h2>
        <p className="text-gray-700 text-base">{description}</p>
        <p>
          <strong>{price} $</strong>
        </p>
        <Button onClick={() => addToShoppingCart(id, title, price)}>Add to Cart</Button>
      </div>
      <div className="px-6 pt-4 pb-2">
        <Budget text='photography' />
        <Budget text='travel' />
        <Budget text='winter' />
      </div>
      <Link to={`/products${id}`}>More info</Link>
    </div>
  );
}
