import H1 from "../../Headings/H1";

export default function ErrorPage() {
  return (
    <>
      <H1 title="Error" />
    </>
  );
}
