import Card from "../../components/Card";

export default function ProductDetail() {

  return (
    <>
      <section className='w-full flex'>
        <article className='flex flex-col w-[80%]'>
          <Card
            id='1'
            key='1'
            title='Product name'
            description='Product description'
            price='100.20'
            css='rounded overflow-hidden shadow-lg mr-5'
          />
        </article>
        <aside className='flex flex-col right-0 border rounded shadow-lg w-[20%] p-5'>
          <h4>Extra info</h4>
        </aside>
      </section>
    </>
  )
}
