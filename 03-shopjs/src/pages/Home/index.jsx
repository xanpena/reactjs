import CardList from "../../components/CardList";
import Title from "../../components/Title";
import Card from "../Card";
import useProducts from "../../hooks/useProducts";


export default function Home() {

  const {products} = useProducts()

  return (
    <>
      <Title text="Home" />
      <CardList>
        {
          products?.map(product =>
            <Card
              id={ product.id }
              key={ product.id }
              title={ product.title }
              description={ product.description }
              price={ product.price }
            />
          )
        }
      </CardList>
    </>
  );
}
