import { useContext } from "react";
import Context from "../../context/ShoppingCartContext";
import H1 from "../../Headings/H1";

export default function Orders() {
  const { shoppingCart, totalPrice } = useContext(Context);

  return (
    <>
      <H1 title="My orders" />
      <section>
        <article>
          <table>
            <thead>
              <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Quantity</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {shoppingCart?.map((product) => (
                <tr key={product.id}>
                  <td>{product.id}</td>
                  <td>{product.title}</td>
                  <td>{product.quantity}</td>
                  <td>{product.price} $</td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <td colspan="3">Total</td>
                <td>{totalPrice} $</td>
              </tr>
            </tfoot>
          </table>
        </article>
      </section>
    </>
  );
}
