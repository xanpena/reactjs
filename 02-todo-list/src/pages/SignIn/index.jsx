import { useContext } from "react";
import Context from './../../context/AuthContext'

export default function SignIn() {

  const { handleSignInOnChange, signIn } = useContext(Context);

  const handleSignInOnSubmit = (event) => {
    event.preventDefault()
    signIn()
  }

  return (
    <>
      <h1>Sign in</h1>

      <form onSubmit={handleSignInOnSubmit}>
        <label>Usuario:</label>
        <input
          onChange={e => handleSignInOnChange(e.target.value)}
        />

        <button type="submit">Entrar</button>
      </form>
    </>
  )
}
