import React from 'react'
import useAuth from './../hooks/useAuth'

const Context = React.createContext({})

export function AuthContextProvider({ children }) {

  const {
    handleSignInOnChange,
    signIn,
    logOut,
    PrivateRoute
  } = useAuth()

  return <Context.Provider value={{
    handleSignInOnChange,
    signIn,
    logOut,
    PrivateRoute
  }}>
    { children }
  </Context.Provider>
}

export default Context
