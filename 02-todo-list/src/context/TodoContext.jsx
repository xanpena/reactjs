import React from 'react'
import useTodos from './../hooks/useTodos'

const Context = React.createContext({})

export function TodoContextProvider({children}) {

  const {
    completeTodo,
    completedTodos,
    createTodo,
    deleteTodo,
    filterTodos,
    handleSearchChange,
    searchValue,
    totalTodos
  } = useTodos()

  return <Context.Provider value={{
    completeTodo,
    completedTodos,
    createTodo,
    deleteTodo,
    filterTodos,
    handleSearchChange,
    searchValue,
    totalTodos
  }}>
    {children}
  </Context.Provider>
}

export default Context
