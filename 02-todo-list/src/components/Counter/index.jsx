import { useContext } from 'react'
import Context from '../../context/TodoContext'

function Counter() {

  const {totalTodos, completedTodos} = useContext(Context)

  return (
    <h2>Has completado {completedTodos} de {totalTodos}</h2>
  )
}

export { Counter }
