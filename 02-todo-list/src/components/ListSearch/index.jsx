import { useContext } from 'react'
import Context from '../../context/TodoContext'

function ListSearch() {

  const {searchValue, handleSearchChange} = useContext(Context)

  return (
    <input
      name="search"
      type="text"
      className="search"
      value={ searchValue }
      onChange={handleSearchChange}
    />
  )
}

export { ListSearch }
