import { ButtonWithIcon } from '../Buttons/ButtonWithIcon'

function ListItem({ text, completed, onComplete, onDelete}) {

    return (
      <li>
        <ButtonWithIcon completed={completed} onComplete={onComplete} />
        <p>{text}</p>
        <button onClick={onDelete}>Borrar</button>
      </li>
    )
}

export { ListItem }
