import { useContext, useState } from 'react'
import Context from '../../context/TodoContext'

function FormCreate({ onClose }) {

  const {createTodo} = useContext(Context)
  const [newTodo, setNewTodo] = useState('')

  const handleOnSubmit = (event) => {
    event.preventDefault()
    createTodo(newTodo)
    onClose()
  }

  const handleOnChange = (event) => {
    setNewTodo(event.target.value)
  }

  return (
    <div className="modal">
      <form onSubmit={handleOnSubmit}>
        <label>Escribe tu nuevo To Do</label>
        <textarea onChange={handleOnChange} />
        <button>Guardar</button>
      </form>
      <button onClick={onClose}>Close</button>
    </div>
  );
}

export  { FormCreate }
