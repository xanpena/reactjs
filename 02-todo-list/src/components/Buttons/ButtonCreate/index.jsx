import { useState } from 'react';
import { createPortal } from 'react-dom';
import { FormCreate } from './../../FormCreate'

function ButtonCreate() {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <button onClick={() => setShowModal(true)}>
        Crear To Do
      </button>
      {showModal && createPortal(
        <FormCreate onClose={() => setShowModal(false)} />,
        document.body
      )}
    </>
  );
}

export  { ButtonCreate }
