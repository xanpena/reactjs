import { IoMdCheckboxOutline } from "react-icons/io";
import { MdOutlineCheckBoxOutlineBlank } from "react-icons/md";


function ButtonWithIcon({completed, onComplete}) {
    return (
      <button className="button button-checkbox" onClick={onComplete}>{(completed) ? <IoMdCheckboxOutline /> : <MdOutlineCheckBoxOutlineBlank />}</button>
    )
}

export { ButtonWithIcon }
