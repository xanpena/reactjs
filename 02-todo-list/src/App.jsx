import './App.css'
import { HashRouter, Routes, Route } from 'react-router-dom';
import { useContext } from 'react'
import { Counter } from './components/Counter'
import { ButtonCreate } from './components/Buttons/ButtonCreate'
import { ListSearch } from './components/ListSearch'
import { List } from './components/List'
import { ListItem } from './components/ListItem'
import Context from './context/TodoContext'
import Home from './pages/Home'
import TaskList from './pages/TaskList'
import Task from './pages/Task'
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'
import Profile from './pages/Profile'
import { AuthContextProvider } from './context/AuthContext';

function App() {

  const { filterTodos, completeTodo, deleteTodo } = useContext(Context)

  return (
    <>
      <HashRouter>
        <AuthContextProvider>
          <Routes>
            <Route path="/" element={<Home />} />

            <Route path="/blog" element={<TaskList />}>
              <Route path=":slug" element={<Task />} />
            </Route>

            <Route path="/sign-in" element={<SignIn />} />
            <Route path="/sing-up" element={<SignUp />} />
            <Route path="/profile" element={<PrivateRoute><Profile /></PrivateRoute>} />

            <Route path="*" element={<p>Not found</p>} />
          </Routes>
          <h1>To Do List</h1>
          <Counter />
          <ListSearch />

          <List>
            {filterTodos.map(item => (
              <ListItem
                key={ item.id }
                text={ item.text }
                completed={ item.completed }
                onComplete={() => completeTodo(item.id)}
                onDelete={() => deleteTodo(item.id)}
              />
            ))}
          </List>

          <ButtonCreate />
        </AuthContextProvider>
      </HashRouter>
    </>
  )
}

export default App

