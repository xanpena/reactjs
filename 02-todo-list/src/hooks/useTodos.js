import {useState} from 'react'
import useLocalStorage from './useLocalStorage'

export default function useTodos() {

  const { todoList, saveInLocalStorage } = useLocalStorage()
  const [searchValue, setSearchValue] = useState('')

  const handleSearchChange = event => {
    setSearchValue(event.target.value)
  }

  const completeTodo = (id) => {
    const newTodoList = [...todoList]
    newTodoList.find(item => item.id == id).completed = !newTodoList.find(item => item.id == id).completed
    saveInLocalStorage(newTodoList)
  }

  const deleteTodo = (id) => {
    const newTodoList = [...todoList]
    const todoIndex = newTodoList.findIndex(item => item.id == id)
    newTodoList.splice(todoIndex, 1)
    saveInLocalStorage(newTodoList)
  }

  const createTodo = (newTodo) => {
    const newTodoList = [...todoList]
    newTodoList.push({
      id: 5,
      text: newTodo,
      completed: false
    })
    saveInLocalStorage(newTodoList)
  }

  const totalTodos = todoList.length;
  const completedTodos = todoList.filter(item => item.completed).length;
  const filterTodos = todoList.filter(item => {
    return item.text.toLowerCase().includes(searchValue.toLocaleLowerCase())
  })

  return {
    completeTodo,
    completedTodos,
    createTodo,
    deleteTodo,
    filterTodos,
    handleSearchChange,
    searchValue,
    totalTodos
  }
}
