import {useState} from 'react'

export default function useLocalStorage() {

  let parsedTodos = JSON.parse(localStorage.getItem('TODOS_V1'))
  const defaultTodos = [
    {id: 1, text: 'Figma', completed: true },
    {id: 2, text: 'React', completed: false },
    {id: 3, text: 'Python', completed: false },
    {id: 4, text: 'Task 4', completed: false }
  ]

  const [todoList, setTodoList] = useState(parsedTodos ?? defaultTodos);

  const saveInLocalStorage = (newTodoList) => {
    localStorage.setItem('TODOS_V1', JSON.stringify(newTodoList))
    setTodoList(newTodoList)
  }

  return { todoList, saveInLocalStorage }
}
