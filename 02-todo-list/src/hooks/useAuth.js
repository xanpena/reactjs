import { useState } from 'react';
import { useNavigate } from 'react-router-dom';


export default function useAuth() {

  const [username, setUserName] = useState(null);
  const navigate = useNavigate();

  const handleSignInOnChange = () => {
    setUserName({ username})
  }

  const signIn = () => {
    navigate('/profile')
  }

  const logOut = () => {

  }

  const PrivateRoute = (props) => {
    if (!username) {
      return  navigate('/login');
    }

    return props.children
  }

  return {
    handleSignInOnChange,
    signIn,
    logOut,
    PrivateRoute
  };
}
