import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

test('renders without crashing', () => {
  render(<App />);
  const linkElement = screen.getByText(/Gifs populares/i);
  expect(linkElement).toBeInTheDocument();
});

test('search form could be used', async () => {
  render(<App />)
  const input = await screen.findByRole('textbox')
  fireEvent.change(input, { value: 'Ragnar' })
  const button = await screen.findByRole('button')
  fireEvent.click(button)
})