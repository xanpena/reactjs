import React from 'react'
import './App.css';
import Home from './pages/Home'
import Detail from './pages/Detail'
import ListOfGifs from './components/ListOfGifs';
import { Route } from "wouter"

function App() {
  return (
    <div className="App">
      <section className="App-content">
        <Route
          component={Home}
          path="/"
        />
        <Route
          component={ListOfGifs}
          path="/search/:keyword" 
        />
        <Route
          component={Detail}
          path="/gif/:id"
        />
      </section> 
    </div>
  );
}

export default App
