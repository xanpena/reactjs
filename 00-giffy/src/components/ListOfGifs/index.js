import React from 'react'
import Gif from '../Gif';
import useGifs  from '../../hooks/useGifs';
import debounce from 'just-debounce-it'

export default function ListOfGifs({params}) {

    const { keyword } = params
    const { loading, gifs } = useGifs({ keyword })

    const debounceHandleNextPage = () => debounce(
        () => console.log('next page'), 100
    )

    return <div className="ListOfGifs">
        {
            gifs.map(singleGif => 
                <Gif
                    id={singleGif.id}
                    key={singleGif.id}
                    title={singleGif.title}
                    url={singleGif.url}
                />
            )
        }
    </div>
}