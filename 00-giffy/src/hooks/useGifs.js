import {useState, useEffect} from 'react'
import getGifs from '../services/getGifs';

export default function useGifs({ keyword } = { keyword: null }) {
  const [loading, setLoading] = useState(false)
  const [gifs, setGifs] = useState([])
  const keywordToUse = keyword ? keyword : localStorage.getItem('lastKeyword')

  useEffect(function() {
    setLoading(true)

    getGifs({ keyword: keywordToUse })
      .then(gifs => {
        setGifs(gifs)
        setLoading(false)
        localStorage.setItem('lastKeyword', keyword)
      })
  }, [keyword])

  return {loading, gifs}
}