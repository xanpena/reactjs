import React, { useCallback } from 'react'
import { Link, useLocation } from "wouter"
import SearchForm from '../../components/SearchForm'

export default function Home() {
    const [path, pushLocation] = useLocation()

    const handleSubmit = useCallback(({keyword}) => {
        pushLocation(`/search/${keyword}`)
    }, [pushLocation])
    
    return (
        <>
            <h3 className="App-title">Gifs populares</h3>
            <SearchForm onSubmit={handleSubmit}/>
            <ul>
                <li><Link to='/'>Home</Link></li>
                <li><Link to='/search/ragnar'>Gifs de Ragnar</Link></li>
                <li><Link to='/search/mcgregor'>Gifs de Mcgregor</Link></li>
                <li><Link to='/search/goku'>Gifs de Goku</Link></li>
            </ul>
        </>
    );
}