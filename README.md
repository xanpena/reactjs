# React

React JS

## Getting started

- https://legacy.reactjs.org/docs/cdn-links.html
- https://babeljs.io/docs/babel-standalone
- https://react.dev/learn/start-a-new-react-project
- https://create-react-app.dev/
- https://hyper.is/
- https://nextjs.org/
- https://developers.giphy.com/
- https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens
- https://github.com/molefrog/wouter

- https://vitejs.dev/
- https://swc.rs/
- https://unavatar.io/
