import { useState } from 'react'
import './TwitterFollowCard.css'

export function TwitterFollowCard({ username, name, initialIsFollowing }) {
    const [isFollowing, setIsFollowing] = useState(initialIsFollowing)
    const followButtonText = isFollowing ? 'Siguiendo' : 'Seguir'
    const followButtonClass = isFollowing ? 'following' : ''

    const handleClick = () => {
        setIsFollowing(!isFollowing)
    }
    
    return (
        <article className="tw-card">
          <header>
            <img 
                alt="Avatar Goku" 
                src={`https://unavatar.io/${username}`} />
            <div>
              <strong>{name}</strong>
              <span>@{username}</span>
            </div>
          </header>
    
          <aside>
            <button className={followButtonClass} onClick={handleClick}>
                {followButtonText}
            </button>
          </aside>
        </article>
    )
}