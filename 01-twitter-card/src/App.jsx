import viteLogo from '/vite.svg'
import './App.css'
import { TwitterFollowCard } from './Components/TwitterFollowCard'

function App() {

  return (
    <div>
      <TwitterFollowCard initialIsFollowing username='goku' name="Goku" />
      <TwitterFollowCard username='vegeta' name="Vegeta" />
      <TwitterFollowCard username='celula' name="celula" />
    </div>
  )
}

export default App
